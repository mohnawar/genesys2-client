/**
 * Copyright 2013 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.ecpgr.eurisco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ecpgr.eurisco.JdbcRunner.JdbcRowHandler;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.OAuthAuthenticationException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public abstract class EuriscoGenesysUpdater implements JdbcRowHandler {

	private static final Log _log = LogFactory
			.getLog(EuriscoGenesysUpdater.class);

	protected ObjectMapper mapper = new ObjectMapper();
	ArrayList<ObjectNode> list = new ArrayList<ObjectNode>();
	private String batchInstCode = null;

	private int totalCount = 0;

	@Override
	public boolean handleRecord(ResultSet rs) throws SQLException {
		String instCode = rs.getString(1);
		String acceNumb = rs.getString(2);
		String genus = rs.getString(3);

		ObjectNode aid3 = GenesysClient.makeAid3(instCode, genus, acceNumb);

		addToJson(rs, aid3);
		_log.debug(aid3);

		enqueue(instCode, aid3);

		// Do read next record! Returning false would
		// break
		// the loop.
		return true;
	}

	/**
	 * Send data currently in list to Genesys under batchInstCode.
	 */
	@Override
	public void flush() {
		if (batchInstCode == null || this.list.size() == 0)
			// Nothing to send
			return;

		// Send batch to Genesys
		ArrayList<ObjectNode> batchCopy = new ArrayList<ObjectNode>(this.list);

		String result = sendToGenesys(batchInstCode, batchCopy);
		_log.info(result);

	}

	private void enqueue(String instCode, ObjectNode aid3) {
		// Check if aid3 is for the same batch
		if (this.list.size() >= Runner.BATCH_SIZE
				|| !instCode.equals(batchInstCode)) {

			_log.info("BATCH_SIZE reached, batchInstCode=" + batchInstCode);

			flush();
			this.list.clear();
		}

		batchInstCode = instCode;
		this.list.add(aid3);
	}

	protected abstract void addToJson(ResultSet rs, ObjectNode aid3)
			throws SQLException;

	private String sendToGenesys(String instCode, Collection<ObjectNode> batch) {
		try {
			totalCount += batch.size();
			_log.info("Sending " + batch.size()
					+ " to Genesys... Total sent in this run " + totalCount);
			return genesysJsonCall(instCode, batch);
		} catch (OAuthAuthenticationException e) {
			_log.error(e.getMessage(), e);
			return null;
		}
	}

	protected abstract String genesysJsonCall(String instCode,
			Collection<ObjectNode> batch) throws OAuthAuthenticationException;
}