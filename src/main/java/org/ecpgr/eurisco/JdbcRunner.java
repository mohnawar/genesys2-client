/**
 * Copyright 2013 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.ecpgr.eurisco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JdbcRunner {

	public static interface JdbcRowHandler {
		/**
		 * Handle one entry from EURISCO JDBC reader
		 * 
		 * @throws SQLException
		 * @param rs
		 *            ResultSet
		 * @return false if reader should stop processing
		 */
		boolean handleRecord(ResultSet rs) throws SQLException;

		/**
		 * Reading JDBC is finished, flush things
		 */
		void flush();
	}

	private static final Log _log = LogFactory.getLog(JdbcRunner.class);

	/**
	 * Opens JDBC connection
	 * 
	 * @param url
	 * @param user
	 * @param password
	 * @return null if connection fails
	 */
	protected static Connection open(String url, String user, String password) {
		try {
			Connection connection = DriverManager.getConnection(url, user,
					password);
			connection.setAutoCommit(false);
			return connection;
		} catch (SQLException e) {
			_log.error(e.getMessage(), e);
			return null;
		}
	}

	protected static void close(Connection connection) {
		try {
			_log.info("Closing JDBC");
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			_log.warn(e.getMessage());
		}
	}

	public static void list(Connection connection, int maxRecords,
			String sqlQuery, JdbcRowHandler jdbcRowHandler) throws SQLException {
		PreparedStatement stm = connection.prepareStatement(sqlQuery);

		// Set mysql JConnector to stream results
		stm.setFetchSize(Integer.MIN_VALUE);

		ResultSet rs = stm.executeQuery();

		try {
			int i = maxRecords;

			// Try reading 100 entries
			while (rs.next() && --i > 0 && jdbcRowHandler.handleRecord(rs)) {
				// Doing work ;-)
				// System.out.println("pos=" + i);
			}

			// Limit reached
			jdbcRowHandler.flush();

		} catch (SQLException e) {
			throw e;
		} finally {
			System.out.println("Closing JDBC ResultSet.");
			rs.close();
			stm.close();
		}
	}
}