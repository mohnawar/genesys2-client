/**
 * Copyright 2013 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.ecpgr.eurisco;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.OAuthAuthenticationException;
import org.scribe.exceptions.OAuthConnectionException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Simple reader to fetch MCDP data through JDBC
 * 
 * @author matijaobreza
 * 
 */
public class Runner extends JdbcRunner {

	protected static final Log _log = LogFactory.getLog(Runner.class);
	protected static final int BATCH_SIZE = 50;

	static {
		try {
			// Init mysql driver
			_log.info("Loading MySQL driver.");
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			_log.error("Could not load MySQL JDBC driver class");
		}
	}

	public static void main(String[] args) {
		Connection connection = open(
				"jdbc:mysql://127.0.0.1/eurisco?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false",
				/* username */"root", /* password */"");

		if (connection == null) {
			_log.warn("Could not open JDBC connection.");
			return;
		}
		_log.info("Got JDBC connection!");

		final GenesysClient genesysClient = new GenesysClient();
		genesysClient.loadProperties("client.properties");

		try {
			// checkGenesys(connection, genesysClient);
			setEURISCOMembers(connection, genesysClient);
			upsertAccessions(connection, genesysClient);
			// upsertSomeData(connection, genesysClient);
		} catch (SQLException e) {
			_log.error(e.getMessage(), e);
		} finally {
			// Close JDBC connection
			close(connection);
		}
	}

	/**
	 * Read all rows in table, use EURISCOReader to convert to JsonObject. Check
	 * if accessions exist in Genesys.
	 */
	public static void checkGenesys(final Connection connection,
			final GenesysClient genesysClient) throws SQLException {

		list(connection,
				Integer.MAX_VALUE,
				"SELECT `INSTCODE`, `ACCENUMB`, `GENUS`, `DateOfLastChange` FROM `MCPD` ORDER BY `INSTCODE`",
				new EuriscoGenesysUpdater() {

					@Override
					protected void addToJson(ResultSet rs, ObjectNode aid3)
							throws SQLException {
						// NOP
					}

					@Override
					protected String genesysJsonCall(String instCode,
							Collection<ObjectNode> batch)
							throws OAuthAuthenticationException {
						// Do they exist?
						String response = genesysClient.accessionExists(
								instCode, batch);
						_log.info("Response: " + response);
						int registered = 0, missing = 0;
						try {
							JsonNode resp = mapper.readTree(response);
							ArrayNode arr = (ArrayNode) resp;
							if (arr.size() != batch.size()) {
								_log.warn("Sent " + batch.size()
										+ ", received " + arr.size());
							}
							// returns array
							for (JsonNode r : resp) {
								if (r.has("id") && !r.get("id").isNull()) {
									registered++;
								} else {
									missing++;
									System.err.println("Missing: " + r);
								}
							}
							_log.info("Registered " + registered + ", Missing "
									+ missing);
						} catch (IOException e) {
							_log.error(e.getMessage(), e);
						}
						return response;
					}

				});
	}

	/**
	 * Read all rows in table, use EURISCOReader to convert to JsonObject.
	 * Handle MLSSTAT field as Boolean. Send to Genesys in batches of
	 * BATCH_SIZE.
	 */
	public static void updateMLS(final Connection connection,
			final GenesysClient genesysClient) throws SQLException {

		list(connection,
				Integer.MAX_VALUE,
				// ORDER BY INSTCODE because batches must belong to one INSTCODE
				"SELECT `INSTCODE`, `ACCENUMB`, `GENUS`,`SPECIES`, `MLSSTAT`, `DateOfLastChange` FROM `MCPD` ORDER BY `INSTCODE`",
				new EuriscoGenesysUpdater() {

					@Override
					protected void addToJson(ResultSet rs, ObjectNode aid3)
							throws SQLException {

						String mls = rs.getString(5);

						Boolean inMls = "-".equals(mls) ? null : "1"
								.equals(mls) ? true : false;

						aid3.put("mlsStat", inMls);
					}

					@Override
					protected String genesysJsonCall(String instCode,
							Collection<ObjectNode> batch)
							throws OAuthAuthenticationException {

						return genesysClient.updateMLS(instCode, batch);
					}

				});
	}

	public static void setEURISCOMembers(final Connection connection,
			final GenesysClient genesysClient) throws SQLException {

		ObjectMapper objectMapper = new ObjectMapper();
		final ArrayNode institutes = objectMapper.createArrayNode();

		list(connection, Integer.MAX_VALUE,
				// ORDER BY INSTCODE because batches must belong to one INSTCODE
				"SELECT DISTINCT `INSTCODE` FROM `MCPD` ORDER BY `INSTCODE`",
				new JdbcRowHandler() {

					@Override
					public boolean handleRecord(ResultSet rs)
							throws SQLException {
						institutes.add(rs.getString(1));
						return true;
					}

					@Override
					public void flush() {
					}
				});

		try {
			genesysClient.updateOrganizationMembers("EURISCO", institutes);
		} catch (OAuthAuthenticationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read all rows in table, use EURISCOReader to convert to JsonObject. Send
	 * to Genesys in batches of BATCH_SIZE for insert or/and update;
	 */
	public static void upsertAccessions(final Connection connection,
			final GenesysClient genesysClient) throws SQLException {

		list(connection,
				Integer.MAX_VALUE,
				// ORDER BY INSTCODE because batches must belong to one INSTCODE
				"SELECT `INSTCODE`, `ACCENUMB`, `GENUS`, `SPECIES`, `MLSSTAT`, `ORIGCTY`, `STORAGE`, `SAMPSTAT`, `ACQDATE`, `DUPLSITE`, `COLLSITE`, `COLLDATE`, `COLLSRC`, `COLLDESCR`, `LATITUDED`, `LONGITUDED`, `ELEVATION` `ACCENAME`, `DateOfLastChange` FROM `MCPD` ORDER BY `INSTCODE`",
				new EuriscoGenesysUpdater() {

					@Override
					protected void addToJson(ResultSet rs, ObjectNode aid3)
							throws SQLException {

						String species = StringUtils.defaultIfBlank(
								rs.getString(4), null);
						if (species != null)
							aid3.put("species", species);

						String mls = rs.getString(5);
						Boolean inMls = "-".equals(mls) ? null : "1"
								.equals(mls) ? true : false;
						aid3.put("mlsStat", inMls);

						String orgcty = StringUtils.defaultIfBlank(
								rs.getString(6), null);
						if (orgcty != null)
							aid3.put("orgCty", orgcty);

						String storage = StringUtils.defaultIfBlank(
								rs.getString(7), null);
						if (storage != null)
							aid3.put("storage", storage);

						String sampstat = StringUtils.defaultIfBlank(
								rs.getString(8), null);
						if (sampstat != null)
							aid3.put("sampStat", Integer.parseInt(sampstat));

						String acqDate = StringUtils.defaultIfBlank(
								rs.getString(9), null);
						if (acqDate != null)
							aid3.put("acqDate", acqDate);

						String dublInst = StringUtils.defaultIfBlank(
								rs.getString(10), null);
						if (dublInst != null)
							aid3.put("dublInst", dublInst);

						/**
						 * Group collecting information
						 */
						ObjectNode collecting = mapper.createObjectNode();

						String collSite = StringUtils.defaultIfBlank(
								rs.getString(11), null);
						if (collSite != null)
							collecting.put("site", collSite);
						String collDate = StringUtils.defaultIfBlank(
								rs.getString(12), null);
						if (collDate != null)
							collecting.put("date", collDate);
						String collSrc = StringUtils.defaultIfBlank(
								rs.getString(13), null);
						if (collSrc != null)
							collecting.put("source", collSrc);
						String collDescr = StringUtils.defaultIfBlank(
								rs.getString(14), null);
						if (collDescr != null)
							collecting.put("remarks", collDescr);

						if (collecting.size() > 0)
							aid3.put("coll", collecting);

						/**
						 * Group geo information
						 */
						ObjectNode geo = mapper.createObjectNode();

						float lat = rs.getFloat(15);
						geo.put("lat", rs.wasNull() ? null : lat);
						float lng = rs.getFloat(16);
						geo.put("lng", rs.wasNull() ? null : lng);
						float alt = rs.getFloat(17);
						geo.put("alt", rs.wasNull() ? null : alt);

						if (geo.size() > 0)
							aid3.put("geo", geo);
					}

					@Override
					protected String genesysJsonCall(String instCode,
							Collection<ObjectNode> batch)
							throws OAuthAuthenticationException {

						for (int i = 0; i < 5; i++) {
							try {
								return genesysClient.updateAccessions(instCode,
										batch);

							} catch (OAuthConnectionException e) {
								if (e.getCause() instanceof ConnectException)
									_log.warn("Connect timeout");
								else
									throw e;
							}
						}

						throw new RuntimeException("All retries failed.");
					}

				});
	}

	/**
	 * Read all rows in table, use EURISCOReader to convert to JsonObject. Send
	 * to Genesys in batches of BATCH_SIZE for insert or/and update;
	 */
	public static void upsertSomeData(final Connection connection,
			final GenesysClient genesysClient) throws SQLException {

		list(connection,
				Integer.MAX_VALUE,
				// ORDER BY INSTCODE because batches must belong to one INSTCODE
				"SELECT `INSTCODE`, `ACCENUMB`, `GENUS`, `ACQDATE`, `DUPLSITE`, `DateOfLastChange` FROM `MCPD` ORDER BY `INSTCODE` LIMIT 5000",
				new EuriscoGenesysUpdater() {

					@Override
					protected void addToJson(ResultSet rs, ObjectNode aid3)
							throws SQLException {
						String acqDate = StringUtils.defaultIfBlank(
								rs.getString(4), null);
						aid3.put("acqDate", acqDate);

						String dublInst = StringUtils.defaultIfBlank(
								rs.getString(5), null);
						aid3.put("dublInst", dublInst);
					}

					@Override
					protected String genesysJsonCall(String instCode,
							Collection<ObjectNode> batch)
							throws OAuthAuthenticationException {

						for (int i = 0; i < 5; i++) {
							try {
								return genesysClient.updateAccessions(instCode,
										batch);
							} catch (OAuthConnectionException e) {
								if (e.getCause() instanceof ConnectException)
									_log.warn("Connect timeout");
								else
									throw e;
							}
						}

						throw new RuntimeException("All retries failed.");
					}

				});
	}
}
