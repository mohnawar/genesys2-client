/**
 * Copyright 2013 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.client.oauth.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.genesys2.client.oauth.OAuthAuthenticationException;
import org.scribe.builder.api.DefaultApi20;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.utils.OAuthEncoder;

public class GenesysApi extends DefaultApi20 {

	public static final String AUTHORIZE_URL = "/oauth/authorize?client_id=%s&client_secret=%s&response_type=code&redirect_uri=%s";
	private static final String TOKEN_ENDPOINT = "/oauth/token";

	private String baseUrl;
	private String authorizeUrl;
	private String scopedAuthorizeUrl;
	private String accessTokenEndpoint;
	private String refreshTokenEndpoint;
	private String clientKey;
	private String clientSecret;

	public GenesysApi(String baseUrl, String clientKey, String clientSecret) {
		this.baseUrl = baseUrl;
		this.authorizeUrl = this.baseUrl + AUTHORIZE_URL;
		this.scopedAuthorizeUrl = this.authorizeUrl + "&scope=%s";
		this.refreshTokenEndpoint = this.baseUrl + TOKEN_ENDPOINT;
		this.accessTokenEndpoint = this.baseUrl + TOKEN_ENDPOINT
				+ "?grant_type=authorization_code";
		this.clientKey = clientKey;
		this.clientSecret = clientSecret;
	}

	@Override
	public String getAccessTokenEndpoint() {
		return this.accessTokenEndpoint;
	}

	public String getRefreshTokenEndpoint() {
		return this.refreshTokenEndpoint;
	}

	@Override
	public String getAuthorizationUrl(OAuthConfig config) {

		return config.hasScope() ? String.format(this.scopedAuthorizeUrl,
				config.getApiKey(), config.getApiSecret(),
				OAuthEncoder.encode(config.getCallback()),
				OAuthEncoder.encode(config.getScope())) : String.format(
				this.authorizeUrl, config.getApiKey(), config.getApiSecret(),
				OAuthEncoder.encode(config.getCallback()));
	}

	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new JsonTokenExtractor();
	}

	public Token getRefreshToken(Token accessToken) {
		Pattern refreshTokenPattern = Pattern
				.compile("\"refresh_token\":\\s*\"(\\S*?)\"");

		Matcher matcher = refreshTokenPattern.matcher(accessToken
				.getRawResponse());
		if (matcher.find()) {
			return new Token(matcher.group(1), "", accessToken.getRawResponse());
		}

		return null;
	}

	/**
	 * http://stackoverflow.com/questions/20044222/spring-security-oauth-2-
	 * implicit-grant-no-support-for-refresh-token
	 * 
	 * /oauth/token?client_id=MyClient&grant_type=refresh_token&client_secret=
	 * mysecret&refresh_token=19698a4a-960a-4d24-a8cc-44d4b71df47b
	 * 
	 * @throws OAuthAuthenticationException
	 */
	public Token getAccessToken(Token refreshToken) {
		OAuthRequest request = new OAuthRequest(getAccessTokenVerb(),
				this.refreshTokenEndpoint);
		request.addQuerystringParameter(OAuthConstants.CLIENT_ID, clientKey);
		request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET,
				clientSecret);
		request.addQuerystringParameter("grant_type", "refresh_token");
		request.addQuerystringParameter("refresh_token",
				refreshToken.getToken());
		Response response = request.send();
		return getAccessTokenExtractor().extract(response.getBody());
	}

}
