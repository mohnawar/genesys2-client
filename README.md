
# Genesys PGR - Gateway to genetic resources

Sample client to access and manage data in Genesys.

## Instructions

Make a copy of "client-sample.properties" file and call it "client.properties".
Edit the client.properties file and adjust the configuration.

At minimum, adjust the following variables:

	client.key=CropHub
	client.secret=0xcafebabe
	base.url=http\://YOUR-GENESYS-SERVER
	api.url=/api/v0

## Running the client

In Eclipse, right-click on the project, select Run as > Java Application... and
select the GenesysClient class.

## Making changes

Make a fork of this project in your own (private) Git repository. 

## Feedback

We're available at helpdesk@genesys-pgr.org. Drop us a line.


